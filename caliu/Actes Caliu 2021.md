# Actes de l'any 2021

## Reunió de gener
Dissabte 9 de gener de 2021, canal Jitsi #caliu de meet.guifi.net. Assistents: Rafael Carreras, Aniol Martí, David Pinilla, Josep Maria Ferrer, Ferran, Orestes Mas, Raimon Carbonell. Es constitueix un grup de treball d'administracions. Es parlarà a la llista de la junta de canviar el lloc per als correus @caliu.cat. Es contacta un grupet de professionals del maquinari lliure per organitzar el Dia de la Llibertat del Maquinari a l'abril. S'ha d'actualitzar el servidor de Caliu, en principi al febrer. Es repassen tiquets oberts.

## Reunió de febrer
Dissabte 6 de febrer de 2021, canal Jitsi #caliu de meet.guifi.net. Assistents: Rafael Carreras, Aniol Martí, David Pinilla, Josep Maria Ferrer, Orestes Mas, Raimon Carbonell. El grup de treball d'administracions ha començat a recopilar casos de discriminació d'usuaris de GNU/Linux a l'administració de la Generalitat de Calalunya. S'enviaran demandes de recopilació de casos a Twitter i Mastodon. Es fa una consulta a CD-Mon per àlies de correu electrònic per @caliu.cat. Es comença a preparar el Dia de la Llibertat del Maquinari amb The Things Network Barcelona per Big Blue Button a l'abril. S'ha engegat el disc dur de còpies de seguretat. S'ha d'actualitzar el servidor de Caliu, s'esperarà a més endavant. Es repassen tiquets oberts. Al març es farà l'Assemblea General.

## Assemblea General de març
Dissabte 6 de febrer de 2021, canal Jitsi #caliu de meet.guifi.net. Assistents:Josep Maria Ferrer, Aniol Martí, Rafael Carreras, Àlex Muntada, Miquel Adroer, David Pinilla, Orestes Mas, Raimon Carbonell. S'aprova l'acta de l'assemblea de 2020. Es presenten els comptes de Caliu i les activitats fetes, així com les activitats dels masovers. Es torna a comentar el canvi d'adreces de correu de Caliu. Es detecta una intrusió al subdomini de patents i s'investiga. Podeu veure l'acta de l'assemblea per a més detalls.

## Reunió de maig
Dissabte 1 de maig de 2021, canal Jitsi #caliu de Guifi.net. Assistents: Miquel Chicano, Rafael Carreras, Josep Maria Ferrer, Orestes Mas, Aniol Martí, David Pinilla. Es valora positivament el Dia de la Llibertat del Maquinari, es farà un correu a la llista per l'eliminació de comptes de Google. Els usuaris que vulguin un àlies, hauran de contactar la junta. Qui vulgui un compte, l'haurà de pagar. Es descarta de moment la creació d'un nou grup de treball. Es fa un seguiment del grup de treball d'administracions i de les feines dels masovers. Hi haurà serveis que es deixaran de prestar. Mourem el projecte de Caliu de GitHub a GitLab.

## Reunió de juny
Dissabte 5 de juny de 2021, canal Jitsi #caliu de Guifi.net. Assistents: Rafael Carreras, Aniol Martí, Alex Muntada, David Pinilla, Orestes Mas.
Es recorden les passes i els terminis pel canvi de domini i de correu. Es fa un seguiment del grup de treball d'administracions. Es fa un seguiment de la feina dels masovers. S'ha eliminat l'accés per contrasenya al servidor, només s'hi pot accedir per clau pública per evitar atacs de força bruta. Es crea el grup caliu-cat al GitLab. S'avisarà a la llista de la proposta de canvi.

## Reunió de juliol
Dissabte 3 de juliol de 2021, canal Jitsi #caliu de Guifi.net. Assistents: Orestes Mas, David Pinilla, Josep Maria Ferrer, Aniol Martí, Rafael Carreras. Es parla del lloc on fer el Dia de la Llibertat del Programari, podria ser a Bocanord, La Farinera del Clot, La Santsenca o el Centre Cívic de Les Corts. S'ha passat el projecte de Caliu a GitLab. La migració a Ghandi s'està efectuant i en breu es faràn els àlies @caliu.cat. Es reactiva el grup d'administracions públiques amb un comunicat sobre els formularis pdf a l'administració de la Generalitat. Ens desitgem unes bones vacances.

## Reunió de setembre
Dissabte 4 de setembre de 2021, canal Jitsi #caliu de Guifi.net. Assistents: Rafael Carreras, Josep Maria Ferrer, Aniol Martí, David Pinilla, Orestes Mas. La graella del DLP està tancada, es publicita per xarxes. Es farà una proposta pels pdf a l'administració de la Generalitat. Es posarà el bot dels ubuntaires al servidor. La pàgina web de Caliu quedarà allotjada al GitLab Pages. S'ha trencat el disc dur de còpies de seguretat. S'estudia obrir un compte Rsync, però l'Orestes s'fereix a canviar-lo per un que té a casa. També es posarà al servidor de Caliu la pàgina Perltuts.com que porta l'Alex.

## Reunió d'octubre
Dissabte 2 d'octubre de 2021, canal Jitsi #caliu de Guifi.net. Assistents: Rafael Carreras, Josep Maria Ferrer, Alex Muntada. Es valora positivament el Dia de la Llibertat del Programari, especialment el lloc que era molt ample i amb un gran equip de so que ens van gravar les xerrades i les van penjar tot just l'endemà. S'hi va reclutar un nou masover a qui se li ha fet una introducció de les eines que es fan servir per administrar. S'ha canviat un cable al servidor que no funcionava correctament. S'ha enllestit l'escrit a la Generalitat de Catalunya per l'ús d'eines que no són lliures a l'hora de signar alguns documents, el president l'haurà d'enviar. S'aprova fer una donació a Riseup, on tenim allotjades les llistes de correu, de 50€. Ho anunciarem a la llista. Es proposarà als masovers de fer una jornada tècnica presencial per enllestir la creació de la nova pàgina web de Caliu.

## Reunió de novembre
Dissabte 6 de novembre de 2021,canal Jitsi #caliu de Guifi.net. Assistents: Rafael Carreras, Josep Maria Ferrer. Ja s'ha enviat la instància a la Generalitat i es comença a preparar el Dia de la Llibertat del Maquinari de l'abril. Es comunica a la llista la donació de Caliu a Riseup.

## Reunió de desembre
Dissabte 4 de desembre de 2021,canal Jitsi #caliu de Guifi.net. Assistents: Josep MAria Ferrer, Rafael Carreras, Aniol Martí. Ja s'ha enviat la instància a la Generalitat i es comença a preparar el Dia de la Llibertat del Maquinari de l'abril. Es fan ajustos al bot dels ubuntaires que tenim al servidor. Es farà la propera Assemblea General al juny, amb canvi de junta directiva.
