# Actes de Caliu 2022

## Reunió de gener de 2022
Dissabte 8 de gener de 2022, canal Jitsi #caliu de meet.guifi.net. Assistents: Rafael Carreras, David Pinilla, Xavier de Pedro, Aniol Martí, Josep Maria Ferrer, Orestes Mas, Alex Muntada. No hi ha cap més notícia de la instància enviada. S'enviarà un correu de reclamació. Es parla d'una nova iniciativa de l'Ajuntament de Barcelona i Xnet, bàsicament amb 5 escoles, per implantar serveis lliures (Moodle, Nexcloud,...). Es contactarà gent per celebrar el Dia de la Llibertat del Maquinari a l'abril. S'ha d'actualitzar el PHP del servidor de Caliu, en principi al febrer. S'hauria d'actualitzar Bullseye. Es proposa de fer servir Netdata.cloud (programari lliure al núvol) per monitoritzar el servidor.

## Reunió de febrer de 2022
Dissabte 5 de febrer de 2022, canal Jitsi #caliu de meet.guifi.net. Assistents: David Pinilla, Aniol Martí, Josep Maria Ferrer, Alex Muntada. Es contacta un espai per celebrar el Dia de la Llibertat del Maquinari a l'abril, sense èxit. S'actualitza el servidor i es troben problemes amb els Mediawiki que hi ha.

## Reunió de març de 2022
Dissabte 5 de març de 2022, canal Jitsi #caliu de meet.guifi.net. Assistents: David Pinilla, Josep Maria Ferrer, Xavier de Pedro, Miquel Chicano, Rafael Carreras. El Xavier anirà a parlar amb Bocanord per veure per què no responen els correus. Es contactaran altres llocs per temptejar, i es mirarà d'organitzar el DLM. Se cercaràtemps per reparar els Mediawiki del servidor. L'Administració ha respost quan se'ls ha contactat mitjançant un formulari web i han respost que són conscients del problema amb els formularis no acceccibles amb programari lliure i que estan treballant en la migració a HTML5. Es constata que ja s'han migrat alguns, el problema és que en són molts. Prioritzen els més usats.
